.. _teamsetup:

============
Adding Teams
============

If you have a team or want to announce a team, then use the following settings.
An example is the **OnTheCouch** channel on Mixer.

+-----------+-----------------------------------------------+---------------------------------------------+
|    Name   | Example                                       | Usage                                       |
+-----------+-----------------------------------------------+---------------------------------------------+
| team      | ``!cb team mixer OnTheCouch #DiscordChannel`` | Toggles adding a team to the creators list. |
+-----------+-----------------------------------------------+---------------------------------------------+
| team list | ``!cb team list``                             | Displays a list of added teams.             |
+-----------+-----------------------------------------------+---------------------------------------------+
