.. _allowconfig:

===================
Allow Configuration
===================

Use the following commands to configure what the bot is allowed to do.
Some of these such as ``!cb allow live`` are the basic commands to get the bot setup.

.. csv-table::
      :file: _static/tables/allow.csv 
      :header-rows: 1 
      :class: longtable
      :widths: 1 1 2

.. note:: The "LiveDiscovery" feature works across all plaforms supported by **CouchBot**.
`Users should register at the CouchBot Website <https://couch.bot/User/Registration>`_

**LiveDiscovery** is a feature of **CouchBot** that removes the need for manually added creators.

+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
| Name                | Example                                    | Usage                                                                                               |
+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
| allow livediscovery | ``!cb allow livediscovery none``           | Prevents the automatic announcing of people with the status of "Streaming".                         |
+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
| allow livediscovery | ``!cb allow livediscovery role @Streamer`` | Allows the automatic announcing of people with the status of "Streaming" and the role of @Streamer. |
+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
| allow livediscovery | ``!cb allow livediscovery all``            | Allows the automatic announcing of people with the status of "Streaming".                           |
+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
