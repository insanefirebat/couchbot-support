.. _channelconfiguration:

=====================
Channel Configuration
=====================

The bot needs you to specify where some of the default channels are.
Run the following commands to let it know!

+-------------------+-----------------------------------------+----------------------------+
| Name              | Example                                 | Usage                      |
+-------------------+-----------------------------------------+----------------------------+
| channel greetings | ``!cb channel greetings #channel-name`` | Sets the greeting channel. |
+-------------------+-----------------------------------------+----------------------------+
| channel goodbyes  | ``!cb channel goodbyes #channel-name``  | Sets the goodbye channel.  |
+-------------------+-----------------------------------------+----------------------------+

The **Live** channel setting **__must__** be enabled to allow streams to announce.
If you are not being announced chances are you missed this command!

+--------------+------------------------------------+-------------------------------------------------------+
| Name         | Example                            | Usage                                                 |
+--------------+------------------------------------+-------------------------------------------------------+
| channel live | ``!cb channel live #channel-name`` | This channel will be where Discovery announcements go |
+--------------+------------------------------------+-------------------------------------------------------+
