.. CouchBot documentation master file, created by
    sphinx-quickstart on Mon Aug 19 17:55:39 2019.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Welcome to CouchBot's documentation!
=============================================

.. toctree::
    :maxdepth: 2
    :caption: Installation Guide:

    couchquick
    registration
    patreon
    examples


.. toctree::
    :maxdepth: 2
    :caption: Advanced Commands:

    approvedadmin
    allowconfig
    streamersettings
    teamsetup
    gamesetup
    roleassignment
    channelconfiguration
    customcommands
    gameandtitlefilters
    messages
    moderation
    utilities
    troubleshooting
