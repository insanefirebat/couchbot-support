.. _roleassignment:

================
Role Assignment
================

Use the following commands to configure self role assignment and autorole assignment

.. caution:: If you are assigning a role, make sure the bot is above the role it is supposed to assign.

+-----------------+------------------------------------------------------+----------------------------------------------------------------+
| Name            | Example                                              | Usage                                                          |
+-----------------+------------------------------------------------------+----------------------------------------------------------------+
| role join       | ``!cb role join @RoleName``                          | Assigns a role when people join your server.                   |
+-----------------+------------------------------------------------------+----------------------------------------------------------------+
| role join reset | ``!cb role join reset``                              | Reset the server join role to nothing.                         |
+-----------------+------------------------------------------------------+----------------------------------------------------------------+
| role add        | ``!cb role add "CouchMe" [add/remove] @RoleTheyGet`` | Adds/Removes a role when someone types "**CouchMe**"           |
+-----------------+------------------------------------------------------+----------------------------------------------------------------+
| role remove     | ``!cb role remove "CouchMe"``                        | Removes the *command* "CouchMe"                                |
+-----------------+------------------------------------------------------+----------------------------------------------------------------+
| role live       | ``!cb role live @RoleTheyGet``                       | The role that gets **assigned** to a person when they go live. |
+-----------------+------------------------------------------------------+----------------------------------------------------------------+
| role list       | ``!cb role list``                                    | Displays a list of roles configured on the bot.                |
+-----------------+------------------------------------------------------+----------------------------------------------------------------+
