.. _customcommands:

===============
Custom Commands
===============

Custom commands allow a versatile way for you to have the bot respond to different terms.
This could be used to advertise your Twitter, display your Discord invite or have the bot ping someone!

+----------------+----------------------------------------------------------+-------------------------------------------------------------------------------------------------------+
|      Name      | Example                                                  | Usage                                                                                                 |
+----------------+----------------------------------------------------------+-------------------------------------------------------------------------------------------------------+
| command add    | ``!cb command add "CouchMe" 5 "You have been Couched!"`` | Creates a command called ``CouchMe`` with a 5 second cooldown that replies *"You have been Couched!"* |
+----------------+----------------------------------------------------------+-------------------------------------------------------------------------------------------------------+
| command remove | ``!cb command remove "CouchMe"``                         | Removes the command, in this instance ``CouchMe``.                                                    |
+----------------+----------------------------------------------------------+-------------------------------------------------------------------------------------------------------+
| command list   | ``!cb command list``                                     | Displays a list of the current commands you have programmed.                                          |
+----------------+----------------------------------------------------------+-------------------------------------------------------------------------------------------------------+
