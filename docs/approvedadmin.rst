.. _approvedadmin:

===============
Approved Admins
===============

.. note:: These commands are toggled, if you want to remove someone from the list, run the command again.

In order for people to manually add and remove content creators to the bot, they must be on the **Approved Admin List**.
They can either be added by role or by individual user, see examples below on how to configure this;

+------------+--------------------------------+----------------------------------------------+
| Name       | Example                        | Usage                                        |
+------------+--------------------------------+----------------------------------------------+
| admin      | ``!cb admin @MattTheDev#0001`` | Adds an individual to admin.                 |
+------------+--------------------------------+----------------------------------------------+
| admin      | ``!cb admin @Developers``      | Adds a role to administrate the bot.         |
+------------+--------------------------------+----------------------------------------------+
| admin list | ``!cb admin list``             | Provides a list of the configured bot admins |
+------------+--------------------------------+----------------------------------------------+
| admins     | ``!cb admins``                 | Provides a list of the configured bot admins |
+------------+--------------------------------+----------------------------------------------+
