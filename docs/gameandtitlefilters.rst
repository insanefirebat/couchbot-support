.. _gameandtitlefilters:

======================
Game and Title Filters
======================

.. note:: These commands are toggled, if you want to remove something from the list, run the command again.

Use the following commands to filter announcements by Game and Title. This will only allow items mentioned on this list to be announced!

+--------------+-----------------------------------------+---------------------------------------------------------------------------------+
|     Name     | Example                                 | Usage                                                                           |
+--------------+-----------------------------------------+---------------------------------------------------------------------------------+
| filter game  | ``!cb filter game mixer "World of*"``   | Creates a game filter on Mixer for any games starting with "World Of"           |
+--------------+-----------------------------------------+---------------------------------------------------------------------------------+
| filter title | ``!cb filter title twitch "World of*"`` | Creates a stream title filter on Twitch for any titles starting with "World Of" |
+--------------+-----------------------------------------+---------------------------------------------------------------------------------+
| filter list  | ``!cb filter list``                     | Displays a list of the current applicable filters.                              |
+--------------+-----------------------------------------+---------------------------------------------------------------------------------+
