.. _gamesetup:

============
Adding Games
============

If you have a game or want to announce a game, then use the following settings.

+-----------+-----------------------------------------------+---------------------------------------------+
|    Name   | Example                                       | Usage                                       |
+-----------+-----------------------------------------------+---------------------------------------------+
| game      | ``!cb game "Last Epoch" #DiscordChannel``     | Toggles adding a game to the creators list. |
+-----------+-----------------------------------------------+---------------------------------------------+
| game list | ``!cb game list``                             | Displays a list of added games.             |
+-----------+-----------------------------------------------+---------------------------------------------+
